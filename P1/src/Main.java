import java.util.Scanner;

public class Main {
	int[] primes;

	// aflu numerele prime pana la n;
	public int getPrimeNumbers(int n) {
		final int MAXSIZE = 1000000;
		char[] p = new char[MAXSIZE];
		int i, j, nr = 1;
		int k = 1;
		primes = new int[n];
		for (i = 3; i <= n; i = i + 2) {
			if (p[i] == 0) {
				nr++;
				for (j = i * i; j <= n; j += i << 1) {
					p[j] = 1;
				}
			}
		}
		primes[0] = 2;
		for (i = 3; i <= n; i = i + 2) {
			if (p[i] == 0) {
				primes[k++] = i;
			}
		}
		return nr;
	}

	// aflu numarul care este suma celor mai multi termeni numere prime
	// consecutive
	public int getMaxim(int a, int b) {
		int nr = getPrimeNumbers(b);
		int index = 0;
		while (primes[index++] < a)
			;
		int max = suma_maxim_consecutive(primes[index]);
		int return_val = primes[index++];
		while (index < nr) {
			int val = suma_maxim_consecutive(primes[index]);
			if (val >= max) {
				max = val;
				return_val = primes[index];
			}
			index++;
		}
		return return_val;
	}

	public int suma_maxim_consecutive(int x) {
		int sum = 0;
		int k = 0;
		int first = 0;
		while (sum < x) {
			sum += primes[k++];
		}
		if (sum == x) {
			return k;
		} else {
			while (primes[k] < x) {
				while (sum > x) {
					sum -= primes[first++];
				}
				if (sum == x) {
					return k - first;
				}
				sum += primes[k++];
			}
		}
		return 0;
	}

	// initial nu am citit ca numerele prime trebuie sa fie consecutive, asa ca
	// am facut un bkt pentru a determina toate sumele pentru un numar
	// si o alegeam pe cea cu cei mai multi termeni
	public int suma_maxim(int x, int sum, int k) {
		int z;
		if (sum > x) {
			return -1;
		} else {

			if (sum == x) {
				return 0;
			} else {
				z = suma_maxim(x, sum + primes[k], k + 1);
				if (z >= 0) {
					return z + 1;
				} else {
					k++;
					while (z == -1 && sum + primes[k] < x) {
						z = suma_maxim(x, sum + primes[k++], k);
					}
					if (z == 0) {
						return -1;
					} else {
						return z + 1;
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.printf("Enter a Value:  ");
		int a = in.nextInt();
		System.out.printf("Enter b Value:  ");
		int b = in.nextInt();
		if (b < a) {
			b = a ^ b;
			a = a ^ b;
			b = a ^ b;
		}
		System.out.println("The result is " + (new Main()).getMaxim(a, b));
		;
	}
}
